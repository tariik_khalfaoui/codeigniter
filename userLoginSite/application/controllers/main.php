<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$this->login();
	}
        public function login() {
           $this->load->view('login');
        }
        public function members(){
            $this->load->view('members');
        }
        public function login_validation(){
           $this->load->library('form_validation');
           
           /*  Rules */
           $this->form_validation->set_rules('email', 'Email', 'required|trim|xxs_clean|callback_validation_credentials');
           $this->form_validation->set_rules('password', 'Password', 'required|md5');
           
           if ($this->form_validation->run()){
               redirect('main/members');
           }else{
               $this->load->view('login');
           }
           /*   echo $_POST['email'].'<br />';  */
           echo $this->input->post('email');   
        }        
        public function validate_credentials(){
        $this->load->model('model_users');
        
            if ($this->model_users->can_log_in()){
                /* session data */
                
                return true;
            }else{
                $this-form_validation-set_message('validation_credentials', 'Incorrect email and/or password.');
                return false;
            }
        }
        
}