<div id="content">
    <?php
    
    $this->load->helper("form");
    echo "<font color='red'>".$validatedMessage."</font>";
    echo validation_errors();
    
    echo form_open("site/send_email"); ?>
    <div id="form-wrapper">
        <table>
            <tr><td>
                    <?php echo form_label("Name: ", "fullName");
                        $data = array(
                            "name" => "fullName",
                            "id" => "fullName",
                            "value" => set_value("fullName"),
                        );?>
                </td><td>
                    <?php echo form_input($data);?>
                </td></tr>
                <tr><td>
                    <?php echo form_label("Email: ", "email");
                        $data = array(
                            "name" => "email",
                            "id" => "email",
                            "value" => set_value("email")
                        );?>
                </td><td>
                    <?php echo form_input($data);?>
                </td></tr>
<tr><td>
                    <?php echo form_label("Message: ", "message");
                        $data = array(
                            "name" => "message",
                            "id" => "message",
                            "value" => set_value("message")
                        );?>
                </td><td>
                    <div id="messageBox">
                        <?php echo form_textarea($data);?>
                    </div>
                </td>
                <td></td>
                <td>
                    <?php echo form_submit("contactSubmit","Submit"); ?>
                </td>
        </table>
        
    <?php echo form_close(); ?>
    </div>  <!--Close for wrapper-->
</div>
