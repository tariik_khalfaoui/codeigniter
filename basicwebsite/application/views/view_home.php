<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Code Igniter Website</title>
    <style>
        body, html{
            margin: 0;
            padding: 0;
        }
        body{
            background-color:#EEE;
        }
        h1, h2, h3, h4, p, a, li, ul {
            font-family: aerial, san-serif;
            color: block;
            text-decoration: none;
        }
        #nav{
            margin:50px auto 0 auto;
            width: 1000px;
            background-color: #888;
            height: 15px;
            padding: 20px;
        }
        #nav ul{
            list-style: none;
            float: left;
            margin: 0 20px;
        }
        #nav a:hover {
            color: green;
        }
        #nav ul li{
            font-size: 16px;
            display: inline;
            padding: 10px;
        }
        #content{
            width: 1000px;
            min-height: 100%;
            margin: 0 auto;
            padding: 20px;
        }
        #footer{
            postion: absolute;
            bottom: 30px;
            width: 400px;
            margin: 0 auto;
            padding: 20px;
        }
        #footer p{
            color: #777;
        }
    </style>
</head>
<body>

    <div id="container">
        <h1>The Homepage</h1>
        <div id="nav">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <div id="content">    
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>

            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
        </div>
    <div id="footer">
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
    </div>
</body>
</html>